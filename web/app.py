from flask import Flask, render_template, request, abort

app = Flask(__name__)

@app.errorhandler(404)
def error_404(e):
    return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(e):
    return render_template("403.html"), 403

@app.route("/"+"<path:page_name>")
def render_static(page_name):
    path = page_name + '/'
    print("executing")
    if (("//" in path) or (".." in path) or ("~" in path)):
        abort(403)
    else:
        try:
            return render_template(path)
        except:
            abort(404)


if __name__ == "__main__":
    app.run(host= '0.0.0.0', port=5000, debug=True)
